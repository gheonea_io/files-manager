#!/usr/bin/env bash
arr=('0.7.0')

i=0

# Loop upto size of array
# starting from index, i=0
for i in "${arr[@]}"
do
    git tag -d $i;
    git push origin :refs/tags/$i;
done

exit 0
