# Media files manager for Laravel

## Installation

* Add to root `composer.json` dependencies `"trushdev/files-manager": "^0.0.4"`
* Add to root `composer.json` : 

```json
 "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.trush-dev.com/laravel/files-manager.git"
    }
  ],
```

* Run `composer install`
* Add `<script src="{{ route('files-manager.js') }}"></script>` to blade template where You want to use plugin

#

## Helper 
### `filesManagerImageById($id, $size = null, $webp = true)`

* `$id` - ID of image
* `size` - size of image, `null` means get original image
* `$webp` - force to get `image/webp` if possible

# Author

[Andrii Trush]()

# Licence

Copyright &copy; 2019, [TDev](https://trush-dev.com). You may not make any copies without the permission of the author.

