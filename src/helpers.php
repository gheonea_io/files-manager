<?php

use Illuminate\Support\Str;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Config;


if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

if (!defined('TDFM_ROOT')) {
    define('TDFM_ROOT', __DIR__ . DS);
}

if (!function_exists('filesManagerFileById')) {
    function filesManagerImageById($id, $size = null, $webp = false, $get_relative = false, $ignore_avif = false)
    {
        /** @var \TDevAgency\FilesManager\ResourceModels\TdfmFile $filesManager */
        $filesManager = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return \TDevAgency\FilesManager\ResourceModels\TdfmFile::find($id);
        });
        if ($filesManager === null) {
            return null;
        }

        if ($webp && $size !== null) {

            $pathInfo = pathinfo($filesManager->original_name);

            if (in_array(Str::lower($pathInfo['extension']), ['jpg', 'jpeg', 'png', 'gif'])) {
                $filesManager->original_name = $pathInfo['filename'] . '.webp';
            }
        }
		
		if(Str::lower($filesManager->file_mime) == 'image/svg+xml'){
            $size = null;
        }

        if($webp && Config::get('tdfm.force_show_avif')===true && !$ignore_avif){
            $pi = pathinfo($filesManager->original_name);
            $f_name = $pi['filename'].'.avif';
            $path = '';
            $r_path = '';
            if ($size === null) {
                $path = public_path('/media/' . $id . '/' . $f_name);
                $r_path = '/media/' . $id . '/';
            } else {
                $path = public_path('media/' . $id . '/' . $size . '/' . $f_name);
                $r_path = 'media/' . $id . '/' . $size . '/';
            }
            if(!empty($path) && file_exists($path)){
                if($get_relative){
                    return $r_path.$f_name;
                } else {
                    return asset($r_path.$f_name);
                }
            }
        }

        if ($size === null) {
            if($get_relative){
                return '/media/' . $id . '/' . $filesManager->original_name;
            } else {
                return asset('media/' . $id . '/' . $filesManager->original_name );
            }
        } else {
            if($get_relative){
                return '/media/' . $id . '/' . $size . '/' . $filesManager->original_name;
            } else {
                return asset('media/' . $id . '/' . $size . '/' . $filesManager->original_name );
            }
        }
    }
}
if (!function_exists('filesSrcSetById')) {
    function filesSrcSetById($id, $size = null, $webp = false, $get_relative = false, $ignore_avif = false)
    {
        /** @var \TDevAgency\FilesManager\ResourceModels\TdfmFile $filesManager */
        $filesManager = Cache::rememberForever('tdfm_file_by_id_' . $id, function () use ($id) {
            return \TDevAgency\FilesManager\ResourceModels\TdfmFile::find($id);
        });
        if ($filesManager === null) {
            return null;
        }
        $files = ['avif'=>'','webp'=>'','orig'=>''];

        if(Config::get('tdfm.force_show_avif')===false || !$webp || $ignore_avif){
            unset($files['avif']);
        }

        foreach ($files as $key => $value) {
            if(Str::lower($filesManager->file_mime) == 'image/svg+xml'){
                $size = null;
            }
            $f_path = '';
            $f_name = $filesManager->original_name;
            $f_mime = $filesManager->file_mime;
            if($key=='avif'){
                if ($size !== null) {
                    $pathInfo = pathinfo($filesManager->original_name);
                    if (in_array(Str::lower($pathInfo['extension']), ['jpg', 'jpeg', 'png', 'gif'])) {
                        $f_name = $pathInfo['filename'] . '.avif';
                        $f_mime = 'image/avif';
                    }
                }
            }
            if($key=='webp'){
                if ($webp && $size !== null) {
                    $pathInfo = pathinfo($filesManager->original_name);
                    if (in_array(Str::lower($pathInfo['extension']), ['jpg', 'jpeg', 'png', 'gif'])) {
                        $f_name = $pathInfo['filename'] . '.webp';
                        $f_mime = 'image/webp';
                    }
                }
            }
            $t_path = public_path('/media/' . $id . '/' . $size . '/' . $f_name);
            if ($size === null) {
                $t_path = public_path('/media/' . $id . '/' . $f_name);
            }
            if(!file_exists($t_path)){
                unset($files[$key]);
                continue;
            }
            if ($size === null) {
                if($get_relative){
                    $f_path = '/media/' . $id . '/' . $f_name;
                } else {
                    $f_path = asset('media/' . $id . '/' . $f_name );
                }
            } else {
                if($get_relative){
                    $f_path = '/media/' . $id . '/' . $size . '/' . $f_name;
                } else {
                    $f_path = asset('media/' . $id . '/' . $size . '/' . $f_name );
                }
            }
            $files[$key] = [$f_path,$f_mime];
        }
        if(empty($files)){
            $files[] = [filesManagerImageById($id, $size, $webp, $get_relative)];
        }
        return $files;
    }
}
if (!function_exists('filesManagerPictureById')) {
    function filesManagerPictureById($id, $size = null)
    {
        $size = $size === null ? 'xl' : $size;
        return \View::make('files-manager::picture')->with('picture', \TDevAgency\FilesManager\Facades\PictureTag::pictureTag($id, $size))->render();
    }
}

if (!function_exists('fileManagerActions')) {
    function fileManagerActions()
    {
        return \View::make('files-manager::js_render')->render();
    }
}

if (!function_exists('tdfm_execute')) {
    function tdfm_execute($cmd, $run_silent=false): string
    {
        $process = Process::fromShellCommandline($cmd);

        $processOutput = '';

        $captureOutput = function ($type, $line) use (&$processOutput) {
            $processOutput .= $line;
        };

        $process->setTimeout(null)->run($captureOutput);

        if ($process->getExitCode() && !$run_silent) {
            $exception = new Exception($cmd . " - " . $processOutput);
            report($exception);

            if(!$run_silent){
                throw $exception;
            }
        }
        return $processOutput;
    }
}
