<?php

namespace TDevAgency\FilesManager\Facades;

use claviska\SimpleImage;
use Illuminate\Support\Facades\Facade;

/**
 * @method static SimpleImage fromFile(string $name)
 * @method static SimpleImage fromString(string $string)
 *
 */
class FmImage extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'fmImage';
    }

}
