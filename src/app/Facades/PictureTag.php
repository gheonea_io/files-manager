<?php
/**
 * Created by PhpStorm.
 * User: andrii
 * Date: 18.01.19
 * Time: 11:53
 */

namespace TDevAgency\FilesManager\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class PictureTag
 * @package TDevAgency\FilesManager\Facades
 *
 * @method static array pictureTag($id, $size = 'xl')
 */
class PictureTag extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'picturetag';
    }

}
