<?php

namespace TDevAgency\FilesManager\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class FilesToPublicCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fm:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(public_path('media'))) {
            File::deleteDirectories(public_path('media'));
        }
        File::link(
            storage_path('app/tdfm/generated'),
            public_path('media')
        );
        File::link(
            storage_path('app/tdfm/original'),
            public_path('media/original')
        );
    }
}
