<?php

namespace TDevAgency\FilesManager\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use TDevAgency\FilesManager\ResourceModels\TdfmFile;

class RenameFilesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fm:rename';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = TdfmFile::all();
        /** @var TdfmFile $file */
        foreach ($files as $file) {
            if ($file->original_name === $file->name) {
                continue;
            }

//            continue if file physically doesn't exists on the server
            if (!Storage::disk('tdfm_original')->exists($file->name)) {
                continue;
            }

            if (Storage::disk('tdfm_generated')->exists($file->id)) {
                xdebug_break();
                $a = 0;
            }
        }
    }
}
