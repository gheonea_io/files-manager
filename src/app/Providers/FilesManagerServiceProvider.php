<?php

namespace TDevAgency\FilesManager\Providers;

use claviska\SimpleImage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use TDevAgency\FilesManager\Commands\ClearGeneratedCommand;
use TDevAgency\FilesManager\Commands\FilesToPublicCommand;
use TDevAgency\FilesManager\Commands\SymlinksCommand;
use TDevAgency\FilesManager\Commands\StorageLinkCommand;
use TDevAgency\FilesManager\Helpers\PictureTagHelper;

class FilesManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            TDFM_ROOT . 'config' . DS . 'tdfm.php' => config_path('tdfm.php'),
        ], 'config');
        $this->publishes([
            TDFM_ROOT . 'public' => public_path('vendor/tdfm'),
        ], 'public');
        $this->publishes([
            TDFM_ROOT . 'public' => resource_path('assets/vendor/tdfm'),
        ], 'resource');
        $this->loadRoutesFrom(TDFM_ROOT . 'routes' . DS . 'routes.php');
        $this->loadMigrationsFrom(TDFM_ROOT . 'database' . DS . 'migrations');

        $this->loadViewsFrom(TDFM_ROOT . 'resources' . DS . 'views', 'files-manager');

        if ($this->app->runningInConsole()) {
            $this->commands([
                StorageLinkCommand::class,
                FilesToPublicCommand::class
            ]);
        }
        $this->commands([
            SymlinksCommand::class,
        ]);
        Blade::directive('datetime', function ($expression) {
            return "<?php echo ($expression)->format('m/d/Y H:i'); ?>";
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            TDFM_ROOT . 'config' . DS . 'tdfm.php', 'tdfm'
        );

        Config::set('filesystems.disks', array_merge(Config::get('filesystems.disks'), Config::get('tdfm.disks')));
        App::singleton('fmImage', function () {
            return new SimpleImage;
        });
        App::singleton('picturetag', function () {
            return new PictureTagHelper;
        });


        Blade::directive('fileManagerActions', function ($expression) {
            return "<?= fileManagerActions(); ?>";
        });

        Blade::directive('picture_tag', function ($expression) {
            eval("\$params = [$expression];");
            list($id, $size) = $params;
            return "<?= filesManagerPictureById($id, '$size'); ?>";
        });
    }
}
