<?php

namespace TDevAgency\FilesManager\ResourceModels;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Storage;
use Kyslik\ColumnSortable\Sortable;

class TdfmFile extends Model
{
    use Sortable;
    public $sortable = [
        'id', 'original_name', 'updated_at', 'file_size'
    ];
    protected $fillable = [
        'user_id',
        'original_name',
        'name',
        'file_size',
        'file_mime',
        'file_width',
        'file_height',
    ];
    protected $appends = [
        'url', 'generated', 'webp'
    ];

    public function getUrlAttribute()
    {
        if (isset($this->attributes['name'])) {
            return $this->attributes['url'] = Storage::disk('tdfm_original')->url($this->attributes['original_name']);
        }
    }

    public function getGeneratedAttribute()
    {
        if (isset($this->attributes['name'])) {
            $generated = [];
            foreach (config('tdfm.sizes') as $size => $value) {
                $generated[$size] = route('tdfm.image', ['size' => $size, 'id' => $this->attributes['id'], 'file' => $this->attributes['original_name']]);
            }
            return $this->attributes['generated'] = $generated;
        }
    }

    public function getWebpAttribute()
    {
        if (isset($this->attributes['name']) && config('tdfm.generate_webp')) {
            $generated = [];
            foreach (config('tdfm.sizes') as $size => $value) {
                $generated[$size] = route('tdfm.image', ['size' => $size,
                    'id' => $this->attributes['id'],
                    'file' => str_replace(['.png', '.jpg', '.jpeg', '.gif'], ['.webp', '.webp', '.webp', '.webp'], $this->attributes['original_name'])
                ]);
            }
            return $this->attributes['generated'] = $generated;
        }
    }

    public function localizations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(TdfmFileLocalization::class);
    }

    public function localization(): HasOne
    {
        return $this->hasOne(TdfmFileLocalization::class);
    }

}
