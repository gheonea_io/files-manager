<?php

namespace TDevAgency\FilesManager\ResourceModels;

use Illuminate\Database\Eloquent\Model;

class TdfmDirectory extends Model
{
    protected $fillable = [
        'tdfm_directory_id',
        'name',
    ];

}
