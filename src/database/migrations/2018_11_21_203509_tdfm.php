<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Tdfm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('tdfm_directory', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('tdfm_directory_id')->nullable()->default(0);
                $table->string('name')->nullable();
                $table->timestamps();
            });
            Schema::create('tdfm_files', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('tdfm_directory_id')->nullable();
                $table->string('original_name')->nullable();
                $table->string('name')->index();
                $table->unsignedInteger('file_size');
                $table->string('file_mime');
                $table->timestamps();
            });
            Schema::create('tdfm_file_localizations', function (Blueprint $table) {
                $table->increments('id');
                $table->char('lang', 10);
                $table->unsignedInteger('tdfm_file_id');
                $table->string('file_title')->nullable();
                $table->string('file_alt')->nullable();
                $table->string('file_description')->nullable();
                $table->timestamps();
            });
            Schema::table('tdfm_directory', function (Blueprint $table) {
                $table->index('tdfm_directory_id');
            });
            Schema::table('tdfm_files', function (Blueprint $table) {
                $table->foreign('tdfm_directory_id')->references('id')->on('tdfm_directory');
            });
            Schema::table('tdfm_file_localizations', function (Blueprint $table) {
                $table->foreign('tdfm_file_id')->references('id')->on('tdfm_files');
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Throwable
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('tdfm_file_localizations');
            Schema::dropIfExists('tdfm_files');
            Schema::dropIfExists('tdfm_directory');
            Schema::enableForeignKeyConstraints();
        });
    }
}
