<picture>
    @forelse($picture['sources'] as $source)
        <source srcset="{{$source['src']}}" type="{{$source['mime']}}"/>
    @empty
    @endforelse
    <img src="{{$picture['src']}}" alt="" @if(config('tdfm.lazy')) loading="lazy" @endif />
</picture>
